<?php
namespace App\Http\Services;

use App\Models\Item;
use DB;

class ItemService
{

    public function busca($filial, $data)
    {
        $id     = $data['id'];
        $busca  = trim($data['busca']);
        $result = Item::leftJoin('item_estoque as e', 'item.id', '=', 'e.id_item')
            ->leftJoin('segmento as s', 'item.id_segmento', '=', 's.id')
            ->leftJoin('grupo as g', 'item.id_grupo', '=', 'g.id')
            ->leftJoin('grupo_sub as u', 'item.id_subgrupo', '=', 'u.id')
            ->leftJoin('fornecedor as f', 'item.id_fornecedor', '=', 'f.id')
            ->where('e.id_filial', '=', $filial)
            ->where(function ($query) use ($busca, $id, $filial) {
                $query->where('item.id', '=', $id)
                    ->orWhere('item.barra_1', 'like', '%' . $busca . '%')
                    ->orWhere('item.barra_2', 'like', '%' . $busca . '%')
                    ->orWhere('item.barra_3', 'like', '%' . $busca . '%')
                    ->orWhere('item.descricao_comp', 'ILIKE', '%' . $busca . '%')
                    ->orWhere('item.marca', 'ILIKE', '%' . $busca . '%')
                    ->orWhere('item.obs', 'ILIKE', '%' . $busca . '%')
                    ->orWhere('f.razao', 'ILIKE', '%' . $busca . '%')
                    ->orWhere('item.referencia', 'ILIKE', '%' . $busca . '%')
                    ->whereRaw("item.id IN
             (SELECT if.id_item
              FROM item_fornecedor IF
              WHERE e.id_filial = " . $filial . "
                AND IF.referencia ILIKE '%" . $busca . "%')");
            })
            ->select(DB::raw('FALSE AS sel,
                e.id_filial,
				item.id,
                item.referencia AS refer,
                item.dh_alt AS DATA,
                item.tipo,
                item.descricao_comp AS descr,
                item.marca,
                item.embalagem AS emb,
                e.estoque_atual AS estoq,
                item.un,
                e.custo_atual AS custo,
                item.ativo AS status,
                e.vl_tab_1 AS tab1,
                e.vl_tab_2 AS tab2,
                e.vl_tab_3 AS tab3,
                e.vl_tab_4 AS tab4,
                e.vl_tab_5 AS tab5,
                e.mg_div_1,
                e.mg_mul_1,
                e.mg_div_2,
                e.mg_mul_2,
                e.mg_div_3,
                e.mg_mul_3,
                e.mg_div_4,
                e.mg_mul_4,
                e.mg_div_5,
                e.mg_mul_5,
                e.vl_tab_0 AS tab0,
                e.mg_div_0,
                s.segmento,
                g.grupo,
                item.pneu_usado,
                u.sub_grupo AS subgrupo,
                item.id_marca,
                item.id_segmento AS id_seg,
                item.id_grupo,
                item.id_subgrupo AS id_sub,
                item.item_descricao AS item_des,
                item.complemento AS complem,
                funcao_estoque_filiais(e.id_item) AS est_emp,
                (COALESCE(e.vl_tab_1, 0.0) * (COALESCE(u.web_prod_opcao, 0.0) / 100)) AS garantia,
                item.cod_pers'))
            ->orderBy('item.item_descricao', 'item.marca', 'item.complemento')
            ->get();

        return $result;
    }
}

// SELECT FALSE AS sel,
// item.id,
// item.referencia AS refer,
// item.data_alt AS DATA,
// item.tipo,
// item.descricao_comp AS descr,
// item.marca,
// item.embalagem AS emb,
// e.estoque_atual AS estoq,
// item.un,
// e.custo_atual AS custo,
// item.status,
// e.vl_tab_1 AS tab1,
// e.vl_tab_2 AS tab2,
// e.vl_tab_3 AS tab3,
// e.vl_tab_4 AS tab4,
// e.vl_tab_5 AS tab5,
// e.mg_div_1,
// e.mg_mul_1,
// e.mg_div_2,
// e.mg_mul_2,
// e.mg_div_3,
// e.mg_mul_3,
// e.mg_div_4,
// e.mg_mul_4,
// e.mg_div_5,
// e.mg_mul_5,
// e.vl_tab_0 AS tab0,
// e.mg_div_0,
// s.segmento,
// g.grupo,
// item.pneu_usado,
// u.sub_grupo AS subgrupo,
// item.id_marca,
// item.id_segmento AS id_seg,
// item.id_grupo,
// item.id_subgrupo AS id_sub,
// item.item_descricao AS item_des,
// item.complemento AS complem,
// funcao_estoque_filiais(e.id_prod) AS est_emp,
// (COALESCE(e.vl_tab_1, 0.0) * (COALESCE(u.web_prod_opcao, 0.0) / 100)) AS garantia,
// item.cod_pers
// FROM item i
// LEFT OUTER JOIN item_estoque e ON item.id = e.id_prod
// LEFT OUTER JOIN segmento s ON item.id_segmento = s.id
// LEFT OUTER JOIN grupo g ON item.id_grupo = g.id
// LEFT OUTER JOIN grupo_sub u ON item.id_subgrupo = u.id
// LEFT OUTER JOIN fornecedor f ON item.id_forn = f.id
// WHERE e.id_filial = :fi
//   AND (item.id = :id
//        OR sem_acentos(item.descricao_comp || ' ' || item.marca || ' '|| COALESCE(item.obs, '')) ILIKE :texto
//        OR sem_acentos(item.referencia) ILIKE :texto
//        OR item.barra_1 ILIKE :texto
//        OR item.barra_2 ILIKE :texto
//        OR item.barra_3 ILIKE :texto
//        OR sem_acentos(item.marca) ILIKE :texto
//        OR item.obs ILIKE :texto
//        OR sem_acentos(item.cod_pers) ILIKE :texto
//        OR f.razao ILIKE :texto
// OR (item.id IN
//       (SELECT if.id_item
//        FROM item_forn IF
//        WHERE e.id_filial = :fi
//          AND IF.referencia ILIKE :texto)))
// ORDER BY item.item_descricao,
//          item.complemento,
//          item.marca
