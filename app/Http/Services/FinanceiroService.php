<?php

namespace App\Services;

use App\Models\FilialParam;
use App\Models\Pagar;
use App\Models\Pedido;
use App\Models\Receber;
use DB;

class FinanceiroService
{
    public function pedidos($id_filial, $id_colaborador)
    {
        return Pedido::select('data_faturado')
            ->whereIn('id_vendedor', $id_colaborador)
            ->where('id_filial', $id_filial)
            ->get();
    }

    public function faturados($id_filial, $data)
    {
        // SELECT data_faturado, COUNT(id) AS qtd, SUM(tt_liquido) AS valor, SUM(tt_custo) AS custo, SUM(tt_bonif) AS bonif
        // FROM pedido
        // WHERE excluido = false
        // AND sem_fatura = false
        // AND agrupado <> 'M'
        // AND id_filial = 1
        // AND data_faturado BETWEEN :ini
        // AND :fim AND id_vendedor
        // IN(????) GROUP BY data_faturado ORDER BY data_faturado

        $id_colaboradores = $data['id_colaboradores'];
        $faturado         = $data['faturado'];
        $ini              = $data['data_ini'];
        $fim              = $data['data_fim'];

        if ($faturado) {
            $pedido = Pedido::where('excluido', 'false')
                ->where('sem_fatura', 'false')
                ->where('agrupado', '<>', 'M')
                ->whereIn('id_vendedor', $id_colaboradores)
                ->where('id_filial', $id_filial)
                ->whereNotNull('data_faturado')
                ->whereBetween('data_faturado', [$ini, $fim])
                ->select(DB::raw('data_faturado,COUNT(id) AS qtd, SUM(tt_liquido) AS valor, SUM(tt_custo) AS custo, SUM(tt_bonif) AS bonif'))
                ->orderBy('data_faturado')
                ->groupBy('data_faturado')
                ->get();
        } else {

            $pedido = Pedido::where('excluido', 'false')
                ->where('sem_fatura', 'false')
                ->where('agrupado', '<>', 'M')
                ->whereIn('id_vendedor', $id_colaboradores)
                ->where('id_filial', $id_filial)
                ->whereRaw("DATE(dh_inc) BETWEEN '{$ini}' AND '{$fim}'")
                ->select(DB::raw('DATE(dh_inc) AS data_faturado, COUNT(id) AS qtd, SUM(tt_liquido) AS valor, SUM(tt_custo) AS custo, SUM(tt_bonif) AS bonif'))
                ->orderBy('dh_inc')
                ->groupBy('dh_inc')
                ->get();

        }

        return $pedido;
    }

    public function positivados($id_filial, $data)
    {
        //  SELECT COUNT(id_cliente) AS qtd FROM pedido WHERE excluido = false AND sem_fatura = false AND agrupado <> 'M'
        // AND id_filial = 1 AND data_faturado BETWEEN :ini AND :fim AND id_vendedor IN(??) GROUP BY id_cliente

        $id_colaboradores = $data['id_colaboradores'];
        $ini              = $data['data_ini'];
        $fim              = $data['data_fim'];

        $positivados = Pedido::whereIn('id_vendedor', $id_colaboradores)
            ->where('excluido', 'false')
            ->where('sem_fatura', 'false')
            ->where('agrupado', '<>', 'M')
            ->where('id_filial', $id_filial)
            ->whereBetween('data_faturado', [$ini, $fim])
            ->select(DB::raw('COUNT(id_cliente) AS qtd'))
            ->groupBy('id_cliente')
            ->get();

        return $positivados;
    }

    public function comparativoDespesas($data)
    {
        // SELECT SUM(COALESCE(meta_desp_real, 0.0)) AS desp, id_filial '+
        //          'FROM filial_param '+
        //          'WHERE id_filial IN (' + Filial + ') AND ano BETWEEN :aini AND :afim AND mes BETWEEN :mini AND :mfim '+
        //          'GROUP BY id_filial '+
        //          'ORDER BY id_filial

        $id_filial = $data['id_filial'];
        $ini       = $data['data_ini'];
        $fim       = $data['data_fim'];

        $arrIni = explode('-', $ini);
        $arrFim = explode('-', $fim);

        $despesas = FilialParam::select(DB::raw('SUM(COALESCE(meta_desp_real, 0.0)) AS desp, id_filial'))
            ->whereIn('id_filial', $id_filial)
            ->whereBetween('mes', [$arrIni[1], $arrFim[1]])
            ->whereBetween('ano', [$arrIni[0], $arrFim[0]])
            ->groupBy('id_filial')
            ->orderBy('id_filial')
            ->get();

        return $despesas;
    }

    public function comparativoPeriodo($data)
    {
        $id_filial = $data['id_filial'];
        $ini       = $data['data_ini'];
        $fim       = $data['data_fim'];

        // SELECT
        //     id_filial,
        //     SUM(COALESCE(tt_liquido, 0.0) + COALESCE(tt_juros, 0.0) + COALESCE(tt_compra, 0.0)) AS total,
        //     SUM(COALESCE(tt_juros, 0.0)) AS tt_juros,
        //     SUM(COALESCE(tt_compra, 0.0)) AS tt_compra,
        //     SUM(COALESCE(vl_frete, 0.0)) AS tt_frete,
        //     SUM(COALESCE(tt_ipi, 0.0)) + SUM(COALESCE(tt_subs, 0.0) + COALESCE(acresc_valor, 0.0)) AS tt_outras,
        //     SUM(COALESCE(tt_bruto, 0.0)) AS prod, SUM(COALESCE(tt_bruto, 0.0) + COALESCE(tt_servico, 0.0)) AS prod_serv,
        //     SUM(COALESCE(vl_desconto, 0.0)) AS tt_desc,
        //     SUM(COALESCE(tt_servico, 0.0)) AS serv, SUM(COALESCE(tt_custo, 0.0)) AS custo
        // FROM pedido
        // WHERE excluido = false
        //     AND sem_fatura = false
        //     AND agrupado <> ''M''
        //     AND faturado = ''S''
        //     AND data_faturado
        //         BETWEEN :ini AND :fim
        //     AND id_filial IN ()
        // GROUP BY id_filial
        // ORDER BY id_filial

        $comparativoPeriodo = Pedido::select(DB::raw(
            'SUM(COALESCE(tt_liquido, 0.0) + COALESCE(tt_juros, 0.0) + COALESCE(tt_compra, 0.0)) AS total,
            SUM(COALESCE(tt_juros, 0.0)) AS tt_juros,
            SUM(COALESCE(tt_compra, 0.0)) AS tt_compra,
            SUM(COALESCE(vl_frete, 0.0)) AS tt_frete,
            SUM(COALESCE(tt_ipi, 0.0)) + SUM(COALESCE(tt_subs, 0.0) + COALESCE(acresc_valor, 0.0)) AS tt_outras,
            SUM(COALESCE(tt_bruto, 0.0)) AS prod, SUM(COALESCE(tt_bruto, 0.0) + COALESCE(tt_servico, 0.0)) AS prod_serv,
            SUM(COALESCE(vl_desconto, 0.0)) AS tt_desc,
            SUM(COALESCE(tt_servico, 0.0)) AS serv, SUM(COALESCE(tt_custo, 0.0)) AS custo,
            id_filial
            '
        ))
            ->where('excluido', 'false')
            ->where('sem_fatura', 'false')
            ->where('agrupado', '<>', 'M')
            ->whereBetween('data_faturado', [$ini, $fim])
            ->whereIn('id_filial', $id_filial)
            ->groupBy('id_filial')
            ->orderBy('id_filial')
            ->get();

        return $comparativoPeriodo;
    }

    public function demonstracaoResultadoExercicioPagar($data)
    {
        // SELECT plce.descricao AS centro, plcu.descricao AS custo, pc.id_custo,
        // SUM(pc.vl_pago) AS valor, COUNT(pc.id) AS reg
        // FROM pagar p INNER JOIN pagar_parc pc ON p.id = pc.id_pagar
        // LEFT OUTER JOIN pl_ct_cen plce ON pc.id_custo_centro = plce.id
        // LEFT OUTER JOIN pl_ct_cus plcu ON pc.id_custo = plcu.id
        // WHERE (p.id_filial IN (1,2,3,4,5,6,7)) AND pc.agrupado NOT LIKE 'S'
        // AND (pc.pago LIKE 'S')
        // AND (' +TpDtPg + ' BETWEEN :ini AND :fim)
        // GROUP BY plce.descricao, plcu.descricao, pc.id_custo
        // ORDER BY plce.descricao, plcu.descricao
        // 0: TpDtPg := 'pc.dt_pgto';
        // 1: TpDtPg := 'pc.vcto_parc';
        // 2: TpDtPg := 'pc.data_inc';

        $id_filial = $data['id_filial'];
        $data_ini  = $data['data_ini'];
        $data_fim  = $data['data_fim'];
        $tipo      = $data['tipo_periodo'];

        $aPagar = Pagar::select(DB::raw('pl_ct_cen.descricao AS centro, pl_ct_cus.descricao AS custo, pagar_parc.id_custo,
                SUM(pagar_parc.vl_pago) AS valor, COUNT(pagar_parc.id) AS reg'))
            ->join('pagar_parc', 'pagar.id', '=', 'pagar_parc.id_pagar')
            ->leftJoin('pl_ct_cen', 'pagar_parc.id_custo_centro', '=', 'pl_ct_cen.id')
            ->leftJoin('pl_ct_cus', 'pagar_parc.id_custo', '=', 'pl_ct_cus.id')
            ->groupBy('pl_ct_cus.descricao', 'pl_ct_cen.descricao', 'pagar_parc.id_custo')
            ->whereIn('pagar_parc.id_filial', $id_filial)
            ->where('pagar_parc.agrupado', 'NOT LIKE', 'S')
            ->whereBetween($tipo, [$data_ini, $data_fim])
            ->where('pagar_parc.pago', 'LIKE', 'S')
            ->get();

        return $aPagar;
    }

    public function detalheCustoPagar($data)
    {
        // SELECT pc.vl_pago AS valor, dt_pgto AS vcto, p.nome_fornec AS razao
        // FROM pagar p INNER JOIN pagar_parc pc ON p.id = pc.id_pagar
        // LEFT OUTER JOIN pl_ct_cen plce ON pc.id_custo_centro = plce.id
        // LEFT OUTER JOIN pl_ct_cus plcu ON pc.id_custo = plcu.id
        // WHERE (p.id_filial IN (1,2,3,4,5)) AND pc.agrupado NOT LIKE 'S'
        // AND (dt_pgto BETWEEN '2015-01-01' AND '2016-12-01') AND (pc.pago LIKE 'S')
        // AND pc.id_custo = 82
        // ORDER BY p.nome_fornec
        //  0: TpDtPg := 'pc.dt_pgto';
        // 1: TpDtPg := 'pc.vcto_parc';
        // 2: TpDtPg := 'pc.data_inc';

        $id_filial = $data['id_filial'];
        $data_ini  = $data['data_ini'];
        $data_fim  = $data['data_fim'];
        $tipo      = $data['tipo_periodo'];
        $id_custo  = $data['id_custo'];

        $detalhe = Pagar::select('pagar_parc.id_custo as id_detalhe', 'pagar_parc.vl_pago as valor', $tipo . ' as vcto', 'pagar.nome_fornec AS razao')
            ->join('pagar_parc', 'pagar.id', '=', 'pagar_parc.id_pagar')
            ->leftJoin('pl_ct_cen', 'pagar_parc.id_custo_centro', '=', 'pl_ct_cen.id')
            ->leftJoin('pl_ct_cus', 'pagar_parc.id_custo', '=', 'pl_ct_cus.id')
            ->whereIn('pagar.id_filial', $id_filial)
            ->where('pagar_parc.agrupado', 'NOT LIKE', 'S')
            ->whereBetween($tipo, [$data_ini, $data_fim])
            ->where('pagar_parc.pago', 'LIKE', 'S')
            ->where('pagar_parc.id_custo', '=', $id_custo)
            ->orderBy('pagar.nome_fornec')
            ->get();

        return $detalhe;
    }

    public function demonstracaoResultadoExercicioReceber($data)
    {
        // SELECT SUM(r.pago_valor) AS valor, COUNT(r.id) AS reg
        //     FROM receber r
        //     WHERE (r.id_filial IN (' + Filiais + ')) AND (r.pago LIKE ''S'') AND
        //          (' + TpDtRc + ' BETWEEN :ini AND :fim)'
        // 0: TpDtRc := 'r.pago_data';
        // 1: TpDtRc := 'r.vcto';
        // 2: TpDtRc := 'r.data_inc';

        $id_filial = $data['id_filial'];
        $data_ini  = $data['data_ini'];
        $data_fim  = $data['data_fim'];
        $tipo      = $data['tipo_periodo'];

        $aReceber = Receber::select(DB::raw('
            SUM(pago_valor) AS valor, COUNT(receber.id) AS reg
            '))
            ->whereIn('receber.id_filial', $id_filial)
            ->where('receber.pago', 'LIKE', 'S')
            ->whereBetween($tipo, [$data_ini, $data_fim])->get();
        return $aReceber;
    }

    public function detalheCustoReceber($data)
    {
        $id_filial = $data['id_filial'];
        $data_ini  = $data['data_ini'];
        $data_fim  = $data['data_fim'];
        $tipo      = $data['tipo_periodo'];

        $detalhe = Receber::select('pago_valor as valor', $tipo . ' as vcto', 'razao')
            ->whereIn('id_filial', $id_filial)
            ->where('pago', 'LIKE', 'S')
            ->whereBetween($tipo, [$data_ini, $data_fim])->get();
        return $detalhe;
    }
}
