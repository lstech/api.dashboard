<?php

namespace App\Http\Services;

use App\Models\Filial;

class EmpresaService
{
    public function getFiliais()
    {
        $filiais = Filial::select('id', 'razao', 'fantasia', 'identif', 'cnpj')->get();
        return $filiais;
    }
}
