<?php

namespace App\Http\Services;

use GuzzleHttp\Client;

class ConfigDBService {
	public static function getConfigDbByEmpresa($emp) {
		$client = new Client(['base_uri' => 'https://ws.lstech.systems']);
		$response = $client->request('POST', '/exportar', [
			'form_params' => [
				'empresa' => $emp,
				'chave' => 'Fr3mkrTjNT9lLTuJ5TidFDCMtKWYFVkVoEg36',
				'modulo' => 'ls_web',
			],
		]);

		$json = json_decode((string) $response->getBody());

		$config = [
			'driver' => 'pgsql',
			'host' => $json->db->host,
			'port' => $json->db->port,
			'database' => $json->db->db,
			'username' => $json->db->username,
			'password' => $json->db->password,
			'charset' => 'UTF-8',
		];
		return $config;

	}
}
