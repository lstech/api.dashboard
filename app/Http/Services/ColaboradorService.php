<?php

namespace App\Services;

use App\Models\Colaborador;

class ColaboradorService
{
    public function getColaboradores($filial_id)
    {
        $colaboradores = Colaborador::where('funcao_venda', 'true')
            ->where('excluido', 'false')
        // ->where('comissionado', 'S')
            ->select('id', 'nome', 'comissionado', 'filiais', 'meta_vendas', 'meta_positiv')
            ->get();

        // $colaboradores = $this->tratarFilial($colaboradores, $filial_id);

        return $colaboradores;
    }

    public function getMetaColaboradores($ids_colaboradores)
    {
        $meta = Colaborador::select(\DB::raw('SUM(meta_vendas) AS meta, SUM(meta_positiv) AS posit'))
            ->whereIn('id', $ids_colaboradores)
            ->first();

        return $meta;
    }

    private function tratarFilial($colaboradores, $filial_id)
    {
        $return = array();

        foreach ($colaboradores as $colaborador) {

            $colab_filial = explode('|', $colaborador->filiais);

            if (count($colab_filial) > 1) {
                $id_filial = (int) $colab_filial[1];
            } else {
                $id_filial = (int) $colab_filial[0];
            }

            $header_id_filial = (int) $filial_id;
            $colaborador->filiais = $id_filial;
            if ($id_filial == $header_id_filial) {
                $return[] = $colaborador;
            }
        }
        return $return;
    }
}
