<?php

namespace App\Http\Middleware;

use App\Http\Services\ConfigDBService;
use Closure;
use Config;
use DB;

class CheckEmpresa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $config = ConfigDBService::getConfigDbByEmpresa($request->header('empresa'));
        Config::set('database.connections.filial', $config);
        $newdb = DB::connection('filial');
        return $next($request);
    }
}
