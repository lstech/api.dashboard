<?php

namespace App\Http\Controllers;

use App\Http\Services\ItemService;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    public function __construct(ItemService $service)
    {
        $this->service = $service;
    }

    public function busca(Request $request)
    {
        return response()->json($this->service->busca($request->header('FilialId'), $request->all()));
    }

}
