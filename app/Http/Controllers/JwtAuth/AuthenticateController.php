<?php

namespace App\Http\Controllers\JwtAuth;

use App\Http\Controllers\Controller;
use App\Http\Services\AcessoService;
use App\Http\Services\ConfigDBService;
use App\Models\Filial;
use App\Models\Usuario;
use Config;
use DB;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller {

  public function __construct(AcessoService $service) {
    $this->acessoService = $service;
  }

  public function authenticate(Request $request) {
    $usuario = $request->get('usuario');
    $senha = strtoupper(md5($request->get('senha')));
    $empresa = $request->header('Empresa');
    $filial_id = $request->get('filial_id');

    $config = ConfigDBService::getConfigDbByEmpresa($empresa);
    Config::set('database.connections.filial', $config);
    $newdb = DB::connection('filial');

    $credentials = Usuario::where('usuario', $usuario)
        ->where('senha', $senha)
        ->first();

    if (null == $credentials) {
      return $this->error('user_not_found', 'Usuário ou senha inválido(s)');
    }

    $acesso = Filial::select(DB::raw("razao, encode(logo, 'base64')"))
        ->where('id', $filial_id)->with([
          'acesso' => function ($query) use ($usuario) {
            $query->where('usuario_acesso.usuario', $usuario);
          },
        ])
        ->whereHas('acesso', function ($query) use ($usuario) {
          $query->where('usuario_acesso.usuario', $usuario);
        })
        ->first();

    if (null == $acesso) {
      return $this->error('filial_not_found', 'Este usuário não tem acesso a está filial');
    }
    $filial = $acesso['attributes'];
    $acessos = $this->acessoService->getAcessos($filial_id, $usuario);

    //if (false == $acessos->relat_desemp) {
    //  return $this->error('access denied', 'Este usuário não tem permissão para ver relatórios');
    //}

    $nome = $credentials->colab;
    $id = $credentials->id;
    $razao = $filial['razao'];
    $logo = $filial['encode'];
    $imgdata = base64_decode($logo);
    $f = finfo_open();
    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
    $logo = 'data: ' . $mime_type . ';base64,' . $logo;

    try {
      if (!$token = JWTAuth::fromUser($credentials)) {
        return $this->error('user_not_found', 'Usuário ou senha inválido(s)');
      }
    } catch (JWTException $e) {
      return $this->error('could_not_create_token', 'Ocorreu um erro durante a autenticação');
    }

    return response()->json(compact('token', 'nome', 'usuario', 'empresa', 'filial_id', 'id', 'acessos', 'razao', 'logo'));
  }

}
