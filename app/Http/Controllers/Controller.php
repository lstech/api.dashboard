<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function error($type, $message, $statusCode = 400)
    {
        return Response::json([
            'error' => $type,
            'message' => $message,
        ], $statusCode);
    }

    public function errors($errors, $statusCode = 400)
    {
        return Response::json($errors, $statusCode);
    }
}
