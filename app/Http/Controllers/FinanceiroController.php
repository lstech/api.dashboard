<?php

namespace App\Http\Controllers;

use App\Services\FinanceiroService;
use Illuminate\Http\Request;

class FinanceiroController extends Controller
{
    public function __construct(FinanceiroService $service)
    {
        $this->service = $service;
    }

    public function pedidos(Request $request)
    {
        return response()->json($this->service->pedidos($request->header('FilialId'), $request->get('id_colaborador')));
    }

    public function faturados(Request $request)
    {
        return response()->json($this->service->faturados($request->header('FilialId'), $request->all()));
    }

    public function positivados(Request $request)
    {
        return response()->json($this->service->positivados($request->header('FilialId'), $request->all()));
    }

    public function comparativoPeriodo(Request $request)
    {
        return response()->json($this->service->comparativoPeriodo($request->all()));
    }

    public function comparativoDespesas(Request $request)
    {
        return response()->json($this->service->comparativoDespesas($request->all()));
    }

    public function demonstracaoResultadoExercicioPagar(Request $request)
    {
        return response()->json($this->service->demonstracaoResultadoExercicioPagar($request->all()));
    }

    public function detalheCustoPagar(Request $request)
    {
        return response()->json($this->service->detalheCustoPagar($request->all()));
    }

    public function demonstracaoResultadoExercicioReceber(Request $request)
    {
        return response()->json($this->service->demonstracaoResultadoExercicioReceber($request->all()));
    }

    public function detalheCustoReceber(Request $request)
    {
        return response()->json($this->service->detalheCustoReceber($request->all()));
    }

}
