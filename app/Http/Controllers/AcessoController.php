<?php

namespace App\Http\Controllers;

use App\Http\Services\AcessoService;
use Illuminate\Http\Request;

class AcessoController extends Controller {

  public function __construct(AcessoService $service) {
    $this->service = $service;
  }

  public function index(Request $request) {
    return response()->json($this->service->getAcessos($request->input('filial_id'), $request->input('usuario')));
  }

}
