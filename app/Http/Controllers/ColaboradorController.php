<?php

namespace App\Http\Controllers;

use App\Services\ColaboradorService;
use Illuminate\Http\Request;

class ColaboradorController extends Controller
{
    public function __construct(ColaboradorService $service)
    {
        $this->service = $service;
    }

    public function getColaboradores(Request $request)
    {
        \Log::info(['request Filial_id', $request->header('FilialId')]);
        return response()->json($this->service->getColaboradores($request->header('FilialId')));
    }

    public function getMetaColaboradores(Request $request)
    {
        return response()->json($this->service->getMetaColaboradores($request->get('id_colaboradores')));
    }
}
