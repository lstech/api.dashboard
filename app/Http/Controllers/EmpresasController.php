<?php

namespace App\Http\Controllers;

use App\Http\Services\ConfigDBService;
use App\Http\Services\EmpresaService;
use Config;
use DB;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    public function __construct(EmpresaService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $emp = $request->header('Empresa');
        \Log::info(['Empresa requisitante', $emp]);
        if ($emp) {
            $config = ConfigDbService::getConfigDbByEmpresa($emp);
            \Log::info(['Config db empresa', $config]);
            Config::set('database.connections.filial', $config);
            $newdb = DB::connection('filial');

            $filiais = $newdb->select('select id,razao,fantasia,identif,cnpj from filial');

            $data_return = [
                'empresa' => $emp,
                'filiais' => $filiais,
            ];

            \Log::info(['Filiais and empresa reurn', $filiais, $emp]);

            return response()->json($data_return);

        } else {
            return response()->json(['error' => 'Não foi encontrada nenhuma empresa nessa url: ' . $emp], 400);
        }
    }

    public function getFiliais(Request $request)
    {
        return response()->json($this->service->getFiliais());
    }
}
