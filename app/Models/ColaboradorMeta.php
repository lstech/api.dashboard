<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ColaboradorMeta extends Model
{
    protected $connection = 'filial';

    protected $table = 'colab_cm_meta';
}
