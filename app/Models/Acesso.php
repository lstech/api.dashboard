<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acesso extends Model
{
    protected $connection = 'filial';

    protected $table = 'acesso';
}
