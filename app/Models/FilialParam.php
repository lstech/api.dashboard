<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilialParam extends Model
{
    protected $table = 'filial_param';

    protected $connection = 'filial';
}
