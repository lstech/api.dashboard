<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receber extends Model
{
    protected $connection = 'filial';

    protected $table = 'receber';
}
