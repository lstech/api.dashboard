<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagar extends Model
{
    protected $connection = 'filial';

    protected $table = 'pagar';
}
