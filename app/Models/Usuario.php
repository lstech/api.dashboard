<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuario';

    protected $visible = ['usuario', 'colab'];

    protected $connection = 'filial';

    protected $fillable = [
        'usuario',
    ];

    protected $hidden = [
        'senha',
    ];
}
