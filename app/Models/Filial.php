<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filial extends Model
{
    protected $table = 'filial';

    protected $connection = 'filial';

    public function acesso()
    {
        return $this->belongsToMany(Acesso::class, 'usuario_acesso', 'id_filial', 'id_acesso');
    }
}
