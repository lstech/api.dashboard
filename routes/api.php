<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('empresa', 'EmpresasController@index');

Route::group(['middleware' => ['checkEmpresa', 'jwt.auth']], function () {

    Route::get('colaboradores', 'ColaboradorController@getColaboradores');
    Route::get('filiais', 'EmpresasController@getFiliais');
    Route::post('acessos', 'AcessoController@index');

});

Route::group(['prefix' => 'financeiro', 'middleware' => ['checkEmpresa', 'jwt.auth']], function () {
    Route::post('pedidosColaboradores', 'FinanceiroController@pedidos');
    Route::post('faturados', 'FinanceiroController@faturados');
    Route::post('positivados', 'FinanceiroController@positivados');
    Route::post('comparativoPeriodo', 'FinanceiroController@comparativoPeriodo');
    Route::post('comparativoDespesas', 'FinanceiroController@comparativoDespesas');
    Route::post('meta_mes', 'ColaboradorController@getMetaColaboradores');
    Route::post('drePagar', 'FinanceiroController@demonstracaoResultadoExercicioPagar');
    Route::post('detalheCustoPagar', 'FinanceiroController@detalheCustoPagar');
    Route::post('dreReceber', 'FinanceiroController@demonstracaoResultadoExercicioReceber');
    Route::post('detalheCustoReceber', 'FinanceiroController@detalheCustoReceber');
});

Route::group(['prefix' => 'item', 'middleware' => ['checkEmpresa', 'jwt.auth']], function () {
    Route::post('busca', 'ItemController@busca');
});

Route::group(['namespace' => '\JwtAuth'], function () {
    Route::post('auth', 'AuthenticateController@authenticate');
});
